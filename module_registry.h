/* This file is automatically generated, DO NOT EDIT! */
#ifndef MOD_REG_H
#define MOD_REG_H
#include "shared.h"
#include "modules/mem_usage_mod.h"
#include "modules/clock_mod.h"
#define NUM_MOD 2
#define register_mod_left(index, win) \
        if (index > 0) \
            modules[index]->offset = modules[index - 1]->offset + modules[index - 1]->width + MODULE_SPACING; \
        else \
            modules[index]->offset = MODULE_SPACING;

    #define register_mod_right(index, win) \
        if (index < (NUM_MOD - 1)) \
            modules[index]->offset = modules[index + 1]->offset - modules[index]->width - MODULE_SPACING; \
        else \
            modules[index]->offset = win->width - modules[index]->width - MODULE_SPACING; 

    #define register_mod_center(index, win) \
        // TODO

    #define register_mod(index, init, win, pos) \
        modules[index] = init(win); \
        register_mod_##pos(index, win)
#define register_mods(win) \
register_mod(0, mem_usage_mod_init, win, left) \
register_mod(1, clock_mod_init, win, right) 
#endif
