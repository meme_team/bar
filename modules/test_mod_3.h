#ifndef TEST_MOD_3_H
#define TEST_MOD_3_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>

#include "../shared.h"
#include "../module.h"

module_t* test_mod_3_init(win_t *win);

#endif
