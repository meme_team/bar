#ifndef MODULE_BASE_H
#define MODULE_BASE_H

/* Useful drawing functions */

#define DRAW_BORDER(mod, size) \
    cairo_set_line_width (mod->cr, size); \
    cairo_rectangle(mod->cr, size / 2, size / 2, mod->width - (size), mod->height - (size)); \
    cairo_stroke(mod->cr); \

#define DRAW_BACKGROUND(mod) \
    cairo_rectangle(mod->cr, 0, 0, mod->width, mod->height); \
    cairo_fill(mod->cr); \

#define DRAW_LINE_UNDER(mod, size, spacing) \
    cairo_set_line_width (mod->cr, size); \
    cairo_move_to(mod->cr, spacing, mod->height - (size) / 2); \
    cairo_line_to(mod->cr, mod->width - spacing, mod->height - (size) / 2); \
    cairo_stroke(mod->cr); \

#define SET_SOURCE_RAINBOW(mod) \
    cairo_pattern_t *pat = cairo_pattern_create_linear (0.0, 0.0, mod->width, 0.0); \
    cairo_pattern_add_color_stop_rgb (pat, 0.0, 0.58, 0, 0.824); \
    cairo_pattern_add_color_stop_rgb (pat, 1.0 / 6.0, 57.0 / 256, 0, 130.0 / 256); \
    cairo_pattern_add_color_stop_rgb (pat, 2.0 / 6.0, 0, 0, 1); \
    cairo_pattern_add_color_stop_rgb (pat, 3.0 / 6.0, 0, 1, 0); \
    cairo_pattern_add_color_stop_rgb (pat, 4.0 / 6.0, 1, 1, 0); \
    cairo_pattern_add_color_stop_rgb (pat, 5.0 / 6.0, 1, 127.0 / 255, 0); \
    cairo_pattern_add_color_stop_rgb (pat, 1.0, 1, 0, 0); \
    cairo_set_source (mod->cr, pat);


#endif
