#ifndef CLOCK_MOD_H
#define CLOCK_MOD_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>

#include "../shared.h"
#include "../module.h"
#include "module_base.h"


module_t* clock_mod_init();

#endif
