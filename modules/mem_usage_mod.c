#include "mem_usage_mod.h"

static module_t* mem_usage_module;

static char* value = NULL;

//https://stackoverflow.com/questions/4229415/c-sysinfo-returning-bad-values-i686

static char* get_value() {
    if (value != NULL) 
        free(value);

    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    double mem_total = -1;
    double mem_free = -1;
    fp = fopen("/proc/meminfo", "r");
    while ((read = getline(&line, &len, fp)) != -1) {
        if(strstr(line, "MemTotal") != NULL) {
            char *ptr = strtok(line, " ");
            for(int i = 0; i < 1; i++)ptr = strtok(NULL, " ");
            mem_total = atof(ptr);
        }else if(strstr(line, "MemAvailable") != NULL) {
            char *ptr = strtok(line, " ");
            for(int i = 0; i < 1; i++)ptr = strtok(NULL, " ");
            mem_free = atof(ptr);
        }

        if(mem_total != -1 && mem_free != -1) break;

    }
    double mem_used = (mem_total-mem_free)/1024/1024;
    fclose(fp);
    if (line)
        free(line);

    value = malloc(sizeof(double));
    sprintf(value,"Mem usage: %.2f GB", mem_used);
    return value;
}
module_updater_start(win)
    value = get_value();

    CTX_CLEAR(mem_usage_module->cr)

    cairo_save(mem_usage_module->cr);
    cairo_set_source_rgb(mem_usage_module->cr, 1, 1, 1);
    cairo_set_font_size (mem_usage_module->cr, 14);

    cairo_move_to (mem_usage_module->cr, 0, 15);
    cairo_show_text (mem_usage_module->cr, value);

    cairo_restore(mem_usage_module->cr);

module_updater_end(mem_usage_module, win)

    static void *trigger_event(void* args){
        xcb_generic_event_t* e = (xcb_generic_event_t*) args;

        if(e->response_type == 4); //button down
    }


module_t* mem_usage_mod_init(win_t* win) {
    module_init(mem_usage_module, win, 300, 2000)
}

