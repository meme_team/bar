#include "test_mod_3.h"


static module_t* test_module_3;

static char* value = NULL;

static char* get_value() {
    if (value != NULL) 
        free(value);

    //value = malloc(7);
    value = malloc(50); //TODO
    //sprintf(value, "%d-%d-%d %d:%d:%d", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    strcpy(value, "World!");
    return value;
}

static mouse_t* mouse;

module_updater_start(win)
    value = get_value();
    CTX_CLEAR(test_module_3->cr)


    cairo_save(test_module_3->cr);
    cairo_set_source_rgb(test_module_3->cr, 1, 1, 1);
    cairo_set_font_size (test_module_3->cr, 14);
   
    cairo_move_to (test_module_3->cr, mouse->mx, mouse->my);
    cairo_show_text (test_module_3->cr, value);
    
    cairo_restore(test_module_3->cr);

    mouse->module_id = -1;
module_updater_end(test_module_3, win)

static void *trigger_event(void* args){
    mouse_t* m = (mouse_t*) args;
    // Mouse coordinates are relative to the main surface.
    // It is now required to subtract the offset, since I figured out how to "move" surfaces.
    mouse->mx = m->mx - test_module_3->offset;
    mouse->my = m->my;
    mouse->module_id = m->module_id;
}


module_t* test_mod_3_init(win_t* win) {
    mouse=malloc(sizeof(mouse_t));
    module_init(test_module_3, win, 500, 100)
    mouse->mx=test_module_3->offset;
    mouse->my=20;
    mouse->module_id=-1;

}

