#ifndef MEM_USAGE_MOD_H
#define MEM_USAGE_MOD_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>

#include "../shared.h"
#include "../module.h"

module_t* mem_usage_mod_init(win_t *win);

#endif
