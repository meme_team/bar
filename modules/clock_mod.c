#include "clock_mod.h"

static module_t* clock_module;

static char* value = NULL;

int i = 0;
static char* get_value() {
    if (value != NULL) 
        free(value);

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    ssize_t size = snprintf(NULL, 0, "%d-%d-%d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    value = malloc(size + 1);
    snprintf(value, size + 1, "%d-%d-%d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);


    return value;
}

module_updater_start(win)
    value = get_value();

    CTX_CLEAR(clock_module->cr)


    cairo_save(clock_module->cr);

    SET_SOURCE_RAINBOW(clock_module)

    cairo_set_font_size (clock_module->cr, 14);
    cairo_move_to (clock_module->cr, 10, 20);
    cairo_show_text (clock_module->cr, value);

    DRAW_LINE_UNDER (clock_module, 2, 10)

    cairo_restore(clock_module->cr);


module_updater_end(clock_module, win)

static void *trigger_event(void* args){
    

}


module_t* clock_mod_init(win_t* win) {
    module_init(clock_module, win, 160, 500)
}

