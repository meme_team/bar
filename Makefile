CC=gcc
CFLAGS=`pkg-config --cflags cairo xcb x11`
LDFLAGS=`pkg-config --libs cairo xcb xcb-atom xcb-ewmh xcb-randr x11`
FLAGS_EXTRA=-pthread -g

all: clean run

precompile: 
	./module_generator.sh registry
	$(CC) -E module_loader.c $(CFLAGS) $(LDFLAGS) $(FLAGS_EXTRA)

bar: bar.c
	./module_generator.sh registry
	$(CC) -o bar bar.c module_loader.c modules/*.c $(CFLAGS) $(LDFLAGS) $(FLAGS_EXTRA)

run: bar
	./bar

clean:
	rm -f bar
	rm -f test
	rm -rf *.o
