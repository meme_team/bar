#include "bar.h"

static module_t** modules;
static int* offsets;
static mouse_t* mouse;
static int done = 0;

int main(int argc, char *argv[]) {
    setbuf(stdout, NULL);

    win_t win;
    win_init(&win);


    modules = load_modules(&win);

    mouse = malloc(sizeof(mouse_t));
    mouse->mx = 0;
    mouse->my = 0;
    mouse->module_id = -1;

    foreach_mod (module_t *module, modules) {

        module->height = win.height - MODULE_SPACING_BOTTOM - MODULE_SPACING_TOP;

        cairo_rectangle_t *bounds = malloc(sizeof(cairo_rectangle_t));
        bounds->x = 0;
        bounds->y = 0;
        bounds->width = module->width;
        bounds->height = module->height;

        module->surface = cairo_recording_surface_create(CAIRO_CONTENT_COLOR_ALPHA, bounds);
        module->cr = cairo_create(module->surface); 

        pthread_t thread;
        int err;
        err = pthread_create(&thread, NULL, (void *)module->updater, (void *) &win);
        if(err){
            fuck(d, err);
            return 1;
        }
    }


    ticker(&win);

    free(mouse);
    win_deinit(&win);

    return 0;
}

static xcb_randr_screen_size_t *get_screen_sizes(xcb_connection_t *con, xcb_screen_t *screen) {
    xcb_randr_get_screen_info_cookie_t screen_info = xcb_randr_get_screen_info_unchecked(con, screen->root);
    xcb_randr_get_screen_info_reply_t *reply = xcb_randr_get_screen_info_reply(con, screen_info, NULL);
    return xcb_randr_get_screen_info_sizes(reply);
}

static void set_window_screen(win_t *window) {
    xcb_screen_iterator_t iter = xcb_setup_roots_iterator(xcb_get_setup(window->connection));
    for (; iter.rem; --window->scrno, xcb_screen_next(&iter))
        if (window->scrno == 0)
        {
            window->scr = iter.data;
            break;
        }
}

static void find_depth(win_t *window) {
    xcb_depth_iterator_t depth_iter = xcb_screen_allowed_depths_iterator(window->scr);

    while (depth_iter.rem) {
        if (depth_iter.data->depth == 32 && depth_iter.data->visuals_len) {
            window->depth = depth_iter.data;
            break;
        }
        xcb_depth_next(&depth_iter);
    }
}

static void find_visual(win_t *window) {
    xcb_visualtype_iterator_t visual_iter = xcb_depth_visuals_iterator(window->depth);

    while (visual_iter.rem) {
        if (visual_iter.data->_class == XCB_VISUAL_CLASS_TRUE_COLOR) {
            window->visual_type = visual_iter.data;
            break;
        }
        xcb_visualtype_next(&visual_iter);
    }
}


static void set_ewmh(win_t *window) {
    xcb_ewmh_connection_t ewmh_con;
    xcb_ewmh_init_atoms_replies(&ewmh_con, xcb_ewmh_init_atoms(window->connection, &ewmh_con), NULL);
    xcb_ewmh_set_wm_window_type(&ewmh_con, window->win, 1, &ewmh_con._NET_WM_WINDOW_TYPE_DOCK);
    xcb_ewmh_set_wm_state(&ewmh_con, window->win, 1, &ewmh_con._NET_WM_STATE_STICKY);

    xcb_ewmh_set_wm_strut(&ewmh_con, window->win, 0, 0, BAR_HEIGHT, 0);
    xcb_ewmh_wm_strut_partial_t partial_strut =  {
        0, 0, BAR_HEIGHT, 0, 0, 0, 0, 0, 0, window->width - 1, 0, 0
    };
    xcb_ewmh_set_wm_strut_partial(&ewmh_con, window->win, partial_strut);
}

void redraw_surface(win_t *window) {
    // We have to manually set the position again, because window managers like to be difficult
    const uint32_t coords[] = { 0, 0 };
    xcb_configure_window (window->connection, window->win, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, coords);

    CTX_CLEAR(window->main_cr)

    cairo_surface_flush(window->main_surface);
    xcb_flush (window->connection);
}

static void cairo_init(win_t *window) {
    window->main_surface = cairo_xcb_surface_create(
            window->connection, 
            window->win, 
            window->visual_type,
            window->width, 
            window->height 
    ); 
    window->main_cr = cairo_create (window->main_surface);
}

static void win_init(win_t *window) {

    window->connection = xcb_connect(NULL, &window->scrno);

    window->win = xcb_generate_id(window->connection);
    set_window_screen(window);

    xcb_randr_screen_size_t *sizes = get_screen_sizes(window->connection, window->scr);

    window->width = sizes->width;
    window->height = BAR_HEIGHT;

    find_depth(window);
    find_visual(window);

    xcb_colormap_t colormap = xcb_generate_id(window->connection);
    xcb_create_colormap(
            window->connection,
            XCB_COLORMAP_ALLOC_NONE,
            colormap,
            window->scr->root,
            window->visual_type->visual_id);

    unsigned int cw_mask = XCB_CW_COLORMAP | XCB_CW_BORDER_PIXEL;
    unsigned int cw_values[] = { window->scr->white_pixel, colormap };

    xcb_create_window(window->connection, window->depth->depth,
            window->win, window->scr->root,
            0, 0,
            window->width, window->height,
            5,
            XCB_WINDOW_CLASS_INPUT_OUTPUT,
            window->visual_type->visual_id,
            cw_mask,
            cw_values);

    const static uint32_t values[] = {XCB_EVENT_MASK_EXPOSURE       | XCB_EVENT_MASK_BUTTON_PRESS   |
                                    XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION |
                                    XCB_EVENT_MASK_ENTER_WINDOW   | XCB_EVENT_MASK_LEAVE_WINDOW   |
                                    XCB_EVENT_MASK_KEY_PRESS      | XCB_EVENT_MASK_KEY_RELEASE}; 
    xcb_change_window_attributes (window->connection, window->win, XCB_CW_EVENT_MASK, values);

    set_ewmh(window);

    xcb_map_window(window->connection, window->win);


    cairo_init(window);
}

void destroy_module(module_t *module) {
    cairo_destroy(module->cr);
    cairo_surface_destroy(module->surface);
}

static void win_deinit(win_t *window) {
    foreach_mod(module_t* module, modules) {
        destroy_module(module);
    }

    xcb_destroy_window(window->connection, window->win);
}


static void win_handle_events(win_t *win){
    xcb_intern_atom_cookie_t delete_cookie = xcb_intern_atom(win->connection, 0, 16, "WM_DELETE_WINDOW");
    xcb_intern_atom_reply_t* delete_reply = xcb_intern_atom_reply(win->connection, delete_cookie, 0);

    xcb_generic_event_t *e; // https://xcb.freedesktop.org/tutorial/events/
    while ((e = xcb_wait_for_event (win->connection))) {
        switch (e->response_type & ~0x80) {
            case XCB_EXPOSE: {
                                 /* Handle the Expose event type */
                                 xcb_expose_event_t *ev = (xcb_expose_event_t *)e;
                                 /* ... */
                                 redraw_surface(win);

                                 break;
                             }
            case XCB_BUTTON_PRESS: {
                                       /* Handle the ButtonPress event type */
                                       xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;
                                       foreach_mod(module_t *module, modules){
                                            if(mouse->mx < module->offset + module->width && mouse->mx > module->offset){
                                                printf("Mouse button pressed, module ID:%d\n", i);
                                                module->trigger_event(e);
                                                goto LEAVE_LOOP_MOUSE;
                                            }
                                       }
LEAVE_LOOP_MOUSE:
                                       /* ... */

                                       break;
                                   }
            case XCB_MOTION_NOTIFY: {
                                        xcb_motion_notify_event_t *motion = (xcb_motion_notify_event_t *)e;
                                        mouse->mx = motion->event_x+5; // Wierd offset, idk
                                        mouse->my = motion->event_y+5;
                                        //printf ("Mouse moved %d,%d\n", mouse->mx, mouse->my);
                                        foreach_mod(module_t *module, modules){
                                            if(mouse->mx < module->offset + module->width){
                                                mouse->module_id=i;
                                                module->trigger_event(mouse);
                                                goto LEAVE_LOOP_MOUSE_2;
                                            }
                                        }
LEAVE_LOOP_MOUSE_2:

             case XCB_CLIENT_MESSAGE: {
                                          printf("lol\n");
                                          if ((*(xcb_client_message_event_t*)e).data.data32[0] == (*delete_reply).atom) {
                                              printf("Here\n");
                                            done = 1;
                                          }
                                      }

                                        break;
                                    }
            default: {
                         /* Unknown event type, ignore it */
                         break;
                     }
        }
        /* Free the Generic Event */
        free(e);
    }
}


static void ticker (win_t *win){
    while (!done) {
        win_handle_events(win);
    }
}


