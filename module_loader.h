#ifndef MODULE_LOADER_H
#define MODULE_LOADER_H

#include "module.h"
#include "module_registry.h"
#include <stdlib.h>
#include <stdio.h>

module_t** load_modules();

typedef struct {
    int current;
} module_iter_t;

#define foreach_mod(item, array) \
    for (int i = 0, keep = 1; i < NUM_MOD; keep = !keep, i++) \
        for (item = (array) [i]; keep; keep = !keep)
        


#endif
