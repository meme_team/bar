#ifndef BAR_H
#define BAR_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

#include <cairo.h>
#include <cairo-xcb.h>
#include <xcb/xcb.h>
#include <xcb/xcb_image.h>
#include <xcb/xcb_aux.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_ewmh.h>
#include <xcb/xcb_atom.h>
#include <xcb/randr.h>

#include "module.h"
#include "module_loader.h"
#include "util.h"
#include "shared.h"

static void win_init(win_t *win);
static void win_deinit(win_t *win);
static void win_draw(win_t *win);
static void win_handle_events(win_t *win);
static void ticker(win_t *win);
static xcb_randr_screen_size_t *get_screen_sizes(xcb_connection_t *con, xcb_screen_t *screen);
static void set_window_screen(win_t *window);
static void find_visual(win_t *window);
static void set_ewmh(win_t *window);
static void cairo_init(win_t *window);
void destroy_module(module_t *module);
static void* refresh_surface(void *arg);

#endif
