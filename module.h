#ifndef MODULE_H
#define MODULE_H

#include "shared.h"

pthread_mutex_t lock;

typedef struct {
    int width;
    int height;
    int offset;
    int update_time;
    void* (*updater)(void *);
    cairo_surface_t *surface;
    cairo_t *cr;
    void* (*trigger_event)(void *);
} module_t;

static void redraw_module(win_t *window, module_t *module) {
    pthread_mutex_lock(&lock);
    cairo_save(window->main_cr);

    cairo_set_source_surface(window->main_cr, module->surface, module->offset, MODULE_SPACING_TOP);
    cairo_set_operator (window->main_cr, CAIRO_OPERATOR_SOURCE);
    cairo_rectangle(window->main_cr, module->offset, MODULE_SPACING_TOP, module->width, module->height);
    cairo_fill(window->main_cr);

    cairo_restore(window->main_cr);

    xcb_flush (window->connection);
    pthread_mutex_unlock(&lock);
}

static int run = 1;

#define module_updater_start(win) \
    static void *updater(void *args) { \
        win_t* win = (win_t*) args; \
        while (run) { \
            clock_t t; \
            t = clock(); \
            t = clock() - t;

#define module_updater_end(module, win) \
            redraw_module(win, module); \
            double sleep = module->update_time - ((double)t * 1000) / CLOCKS_PER_SEC; \
            if (sleep < 0) \
                sleep = 0; \
            usleep((int)sleep * 1000); \
        } \
    }

#define module_init(module, win, w, u) \
    module = (module_t*) malloc(sizeof(module_t)); \
    module->width = w; \
    module->update_time = u; \
    module->updater = updater; \
    module->trigger_event = trigger_event; \
    return module; 

#endif
