#ifndef SHARED_H
#define SHARED_H

#include <cairo.h>
#include <cairo-xcb.h>
#include <xcb/xcb.h>

/* Configuration options */
#define BAR_HEIGHT 50
#define MODULE_SPACING_BOTTOM 10
#define MODULE_SPACING_TOP 10
#define MODULE_SPACING 10

typedef struct {
  int width, height;
  int scrno;
  xcb_screen_t *scr;
  xcb_connection_t *connection;
  xcb_drawable_t win;
  xcb_visualtype_t *visual_type;
  xcb_depth_t *depth;
  cairo_surface_t *main_surface;
  cairo_t *main_cr;
} win_t;


typedef struct {
    int mx;
    int my;
    int module_id;
} mouse_t;

#define CTX_CLEAR(context) \
    cairo_save(context);  \
    cairo_set_operator (context, CAIRO_OPERATOR_CLEAR); \
    cairo_paint (context); \
    cairo_restore(context);

void redraw_surface(win_t *window);

#endif

